package api

import (
	"errors"
	"log"

	"golang.org/x/net/context"

	pb "gitlab.com/mtso/weather/temperature-svc/gen/proto"
	"gitlab.com/mtso/weather/temperature-svc/temperature"
	"google.golang.org/grpc"
)

type TemperatureServiceServer struct {
	svc temperature.TemperatureService
}

func (srv *TemperatureServiceServer) GetTemperature(
	ctx context.Context,
	req *pb.TemperatureRequest) (*pb.TemperatureResponse, error) {
	log.Printf("Req to temperature svc: %s\n", req.City)

	if req.City.Name == "" {
		return nil, errors.New("Must have at least supply city name.")
	}

	t, err := srv.svc.GetTemperature(temperature.City{
		Name:    req.City.Name,
		Country: req.City.Country,
	})
	if err != nil {
		return nil, err
	}

	temp := &pb.Temperature{
		Temperature: int32(t.Temperature),
		Unit:        t.Unit,
	}

	result := &pb.TemperatureResponse{Temperature: temp}
	return result, nil
}

func NewGrpcServer(server *grpc.Server, svc temperature.TemperatureService) {
	pb.RegisterTemperatureServiceServer(server, &TemperatureServiceServer{svc})
}
