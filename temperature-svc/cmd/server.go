package main

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/mtso/weather/temperature-svc/api"
	"gitlab.com/mtso/weather/temperature-svc/temperature"
	"google.golang.org/grpc"
)

var (
	grpcPort      = getOrFail("GRPC_PORT")
	weatherApiKey = getOrFail("WEATHER_API_KEY")
)

func getOrFail(key string) (value string) {
	value = os.Getenv(key)
	if value == "" {
		log.Fatalf("Environment variable must be set: %s=[%s]", key, value)
	}
	return
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", grpcPort))
	if err != nil {
		panic(err)
	}

	grpcServer := grpc.NewServer()
	api.NewGrpcServer(grpcServer, temperature.TemperatureService{weatherApiKey})
	log.Printf("Starting grpc server on GRPC_PORT=[%s]", grpcPort)
	grpcServer.Serve(lis)
}
