package temperature

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

const (
	weatherApi = "https://api.openweathermap.org/data/2.5/weather"
)

type City struct {
	Name    string
	Country string
}

type Temperature struct {
	Temperature int
	Unit        string
	Location    City
}

type TemperatureService struct {
	ApiKey string
}

type WeatherApiResponse struct {
	Code    int64  `json:"cod"`
	Message string `json:"message"`
	Main    struct {
		Temperature float32 `json:"temp,omitempty"`
	} `json:"main,omitempty"`
	Name string `json:"name,omitempty"`
}

func (svc TemperatureService) GetTemperature(location City) (Temperature, error) {
	queryUrl, err := url.Parse(weatherApi)
	if err != nil {
		return Temperature{}, err
	}

	fmtLocation := location.Name
	if location.Country != "" {
		fmtLocation += "," + location.Country
	}

	// qs := queryUrl.Query()
	// qs.Add("q", location.Name)

	// if location.Country != "" {
	// 	qs.Add("q", location.Country)
	// 	// fmtLocation += "," + location.Country
	// }
	// qs.Add("appid", svc.ApiKey)

	queryUrl.RawQuery = fmt.Sprintf("q=%s&appid=%s", fmtLocation, svc.ApiKey)

	log.Println(queryUrl.String())
	resp, err := http.Get(queryUrl.String())
	if err != nil {
		return Temperature{}, err
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(buf)

	log.Println(buf.String())
	dec := json.NewDecoder(resp.Body)

	apiData := WeatherApiResponse{}
	err = dec.Decode(&apiData)
	if err != nil {
		log.Println(err)
		return Temperature{}, err
	}

	if apiData.Code != 200 {
		log.Printf("API Bad Request: %s\n", err)
		return Temperature{}, errors.New(apiData.Message)
	}

	c := City{Name: apiData.Name}
	t := Temperature{int(apiData.Main.Temperature), "K", c}

	return t, nil
}
