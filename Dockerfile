# FROM buoyantio/emojivoto-svc-base:v1
FROM debian:jessie-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        dnsutils \
        iptables \
        jq \
        nghttp2 \
        ca-certificates \
    && rm -rf /var/lib/apt/lists/*

ARG svc_name

COPY $svc_name/target/ /usr/local/bin/

ENV SVC_NAME $svc_name
ENTRYPOINT cd /usr/local/bin && $SVC_NAME
