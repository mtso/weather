.PHONY: web temperature-svc version

web:
	$(MAKE) -C ./weather-web

temperature-svc:
	$(MAKE) -C ./temperature-svc

deploy-to-minikube:
	$(MAKE) -C weather-web
	$(MAKE) -C temperature-svc
	kubectl delete -f weather.yml || echo "ok"
	kubectl apply -f weather.yml

deploy-to-docker-compose:
	docker-compose stop
	docker-compose rm -vf
	$(MAKE) -C weather-web
	$(MAKE) -C temperature-svc
	docker-compose -f ./docker-compose.yml up -d
