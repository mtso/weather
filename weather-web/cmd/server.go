package main

import (
	"log"
	"os"

	pb "gitlab.com/mtso/weather/weather-web/gen/proto"
	"gitlab.com/mtso/weather/weather-web/web"
	"google.golang.org/grpc"
)

var (
	port               = os.Getenv("WEB_PORT")
	temperatureSvcHost = os.Getenv("TEMPERATURESVC_HOST")
	publicPath         = os.Getenv("PUBLIC_PATH")
)

func main() {
	if port == "" || publicPath == "" || temperatureSvcHost == "" {
		log.Fatalf("Env vars need to be set. WEB_PORT=%s PUBLIC_PATH=%s TEMPERATURESVC_HOST=%s",
			port, publicPath, temperatureSvcHost)
	}

	temperatureSvcConn := openGrpcConnection(temperatureSvcHost)
	temperatureClient := pb.NewTemperatureServiceClient(temperatureSvcConn)
	defer temperatureSvcConn.Close()

	log.Println(web.StartServer(port, publicPath, temperatureClient))
}

func openGrpcConnection(host string) *grpc.ClientConn {
	log.Printf("Connecting to [%s]", host)
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return conn
}
