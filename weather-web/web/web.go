package web

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	pb "gitlab.com/mtso/weather/weather-web/gen/proto"
)

type webApp struct {
	publicPath               string
	temperatureServiceClient pb.TemperatureServiceClient
}

func (a *webApp) indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	markup := `
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="UTF-8">
			<title>Weather</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
		</head>
		<body>
			<div id="app"></div>
			<script type="text/javascript" src="/js" async></script>
		</body>
	</html>`

	fmt.Fprintf(w, markup)
}

func (a *webApp) jsHandler(w http.ResponseWriter, r *http.Request) {
	f, err := ioutil.ReadFile(a.publicPath + "/bundle.js")
	if err != nil {
		http.Error(
			w,
			"Could not find client file",
			http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/javascript")
	fmt.Fprintf(w, string(f))
}

func (app *webApp) temperatureHandler(w http.ResponseWriter, r *http.Request) {
	city := r.FormValue("city_name")
	country := r.FormValue("country_code")
	if city == "" {
		http.Error(w, "Must at least have 'city_name'", http.StatusBadRequest)
		return
	}

	request := &pb.TemperatureRequest{
		City: &pb.City{city, country},
	}

	result, err := app.temperatureServiceClient.GetTemperature(r.Context(), request)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		http.Error(w, "Error", http.StatusInternalServerError)
	}

}

// StartServer creates a webApp and starts listening in given port.
func StartServer(port, publicPath string, temperatureClient pb.TemperatureServiceClient) error {

	app := &webApp{
		publicPath:               publicPath,
		temperatureServiceClient: temperatureClient,
	}

	log.Printf("Starting web server on PORT=[%s]", port)
	http.HandleFunc("/", app.indexHandler)
	http.HandleFunc("/js", app.jsHandler)
	http.HandleFunc("/api/temperature", app.temperatureHandler)
	// http.Handle("/dist/", http.StripPrefix("/dist/", http.FileServer(http.Dir(publicPath))))

	return http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
