const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'js/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'dist/',
    filename: 'bundle.js',
  },
  // TODO: What does externals do?
  // externals: {
  //   cheerio: 'window',
  //   'react/addons': 'react',
  //   'react/lib/ExecutionEnvironment': 'react',
  //   'react/lib/ReactContext': 'react',
  //   'react-addons-test-utils': 'react-dom',
  // },
  resolve: {
    extensions: ['.js', '.jsx', '.css'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: [ 'env', 'stage-0', 'react' ],
        },
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader' },
        ],
      },
      {
        test: /\.(png|svg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: { publicPath: 'dist/' },
          },
        ],
      },
    ],
  },
};
