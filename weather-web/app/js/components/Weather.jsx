import React, { Component } from 'react';
import 'whatwg-fetch';

const loadingImage = 'https://www.drupal.org/files/issues/throbber_12.gif';

const convertKelvinToCelcius = (temp) => {
  return temp - 273.15;
}

const convertKelvinToFahrenheight = (temp) => {
  const celcius = convertKelvinToCelcius(temp);
  return celcius * 1.8 + 32;
}

const defaultCity = 'San Francisco';
const defaultCountry = 'us';

class Weather extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unit: 'F',
      temperature: null,
      city_name: defaultCity,
      country_code: defaultCountry,
      fetch_error: null,
    };
  }

  renderTemperature = () => {
    const { temperature, unit } = this.state;
    if (temperature === null) {
      return null;
    }

    switch (unit) {
      case 'F':
        return `${convertKelvinToFahrenheight(temperature).toFixed(2)} ˚F`;
      case 'C':
        return `${convertKelvinToCelcius(temperature).toFixed(2)} ˚C`;
      default:
        return '';
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { city_name, country_code } = this.state;
    fetch(`/api/temperature?city_name=${city_name}&country_code=${country_code}`)
      .then((resp) => resp.json())
      .then(({ temperature }) => {
        this.setState({
          temperature: temperature.temperature,
          fetch_error: null,
        });
      })
      .catch((err) => {
        this.setState({
          fetch_error: err.message,
        });
      });
  }

  onChangeHandler = (field) => (e) => {
    this.setState({ [field]: e.target.value })
  }

  onClickHandler = (unit) => (e) => {
    this.setState({ unit });
  }

  render() {
    const temperatureString = this.renderTemperature();

    const { unit, city_name, country_code, fetch_error } = this.state;

    const display = (() => {
      if (fetch_error) {
        return (
          <div className='container'>
            <h2>Had trouble fetching weather data: {fetch_error}</h2>
            <button onClick={this.fetchData}>Try Again?</button>
          </div>
        )
      } else if (temperatureString) {
        return (
          <h1
            style={{fontSize: '6em'}}
            className='display-text'>{temperatureString}</h1>
        )
      } else {
        return (
          <h1 className='display-text'>
            <img
              className='loader'
              src={loadingImage}
              style={{height: 24}}
            />
          </h1>
        )
      }
    })();

    const locationForm = (
      <form
        className='input-group mb-1'
        role='group'
        onSubmit={(e) => {
          e.preventDefault();
          this.fetchData();
        }}
      >
        <input
          type='text'
          className='form-control'
          placeholder={defaultCity}
          onChange={this.onChangeHandler('city_name')}
          value={city_name}
        />
        <input
          type='text'
          className='form-control'
          placeholder={defaultCountry}
          onChange={this.onChangeHandler('country_code')}
          value={country_code}
        />
        <div className='input-group-append'>
          <input
            type='submit'
            className='btn btn-outline-secondary'
            value='Update'
            onClick={this.fetchData}
          />
        </div>
      </form>
    );

    return (
      <div className='container'>
        <div className='row' style={{marginTop: '2em', marginBottom: '4em'}}>
          <div className='col-sm-2'>
            <h2>Weather App</h2>
          </div>
          <div className='col-sm-2'>
            <div className='btn-group' role='group'>
              <button
                type='button'
                className='btn btn-secondary'
                disabled={unit === 'F'}
                onClick={this.onClickHandler('F')}
              >˚F</button>
              <button
                type='button'
                className='btn btn-secondary'
                disabled={unit === 'C'}
                onClick={this.onClickHandler('C')}
              >˚C</button>
            </div>
          </div>
          <div className='col-sm-4'>
            { locationForm }
          </div>
        </div>
        <div className='row'>
          <div className='col-sm-1'>
            { display }
          </div>
        </div>
      </div>
    )
  }
}

export default Weather;
