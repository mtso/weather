import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Weather from './components/Weather';

// import '../css/base.min.css';
import '../css/bootstrap.min.css';
// import '../css/theme.min.css';
import '../css/weather.css';

const mount = document.querySelector('#app')

render(
  <BrowserRouter>
    <div className="container">
      <Route path="/" component={Weather} />
    </div>
  </BrowserRouter>,
  mount
);
