.PHONY: package protoc

target_dir := target
# version_file := ../VERSION
# version_tag := $(shell cat ${version_file})
# echo $(version_tag)

protoc:
	protoc -I .. ../proto/*.proto --go_out=plugins=grpc:gen/

clean: 
	rm -rf gen
	rm -rf $(target_dir)
	mkdir -p $(target_dir)
	mkdir -p gen/proto

dep:
	dep ensure

build-container:
	docker build .. -t "mtso/$(svc_name):$(shell cat ../VERSION)" --build-arg svc_name=$(svc_name)

compile:
	GOOS=linux go build -v -o $(target_dir)/$(svc_name) cmd/server.go

package: protoc dep compile build-container

run:
	go run cmd/server.go

test:
	go test ./...
