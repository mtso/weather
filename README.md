# Weather Microservice

## Requirements

- GNU Make 3.81
- `kubectl version: Client Version: v1.8.3, Server Version: v1.8.0`
- `conduit version: Client version: v0.1.3, Server version: v0.1.3`

## Future
- Add a location data service for city and country code autocompletion.

## Required Environment Variables

### `temperature-svc`
- `GRPC_PORT`
- `WEATHER_API_KEY`

### `weather-web`
- `WEB_PORT`
- `TEMPERATURESVC_HOST`
- `PUBLIC_PATH`

## Deploy to Conduit
```
cat weather.yml | conduit inject - --skip-inbound-ports=80 | kubectl apply -f -
```

## Deploy to Docker Compose
```
make deploy-to-docker-compose
```
